﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class UIManagerScript : MonoBehaviour {

	private GameObject playerOne;

	public Canvas attrScreen;
	public GameObject btnHpUp, btnHpDown, btnAtkUp, btnAtkDown, btnDefUp, btnDefDown;
	public GameObject uiHP, uiATK, uiDEF, uiPool;
	public GameObject playerName, txtAvailablePts;
	public GameObject spawner;
	private int localtxtHP, localtxtDEF, localtxtATK;



	void Awake(){
		playerOne = GameObject.FindWithTag("Player");
		attrScreen.enabled = false;
	}

	void Start()
	{
		attrScreen.enabled = true;
		AudioListener.volume = 0;

		uiHP.GetComponent<Text>().text = playerOne.GetComponent<Player>().atr_hp.ToString();
		uiATK.GetComponent<Text>().text = playerOne.GetComponent<Player>().atr_atk.ToString();
		uiDEF.GetComponent<Text>().text = playerOne.GetComponent<Player>().atr_def.ToString();
		uiPool.GetComponent<Text>().text = playerOne.GetComponent<Player>().pool.ToString();

		btnHpDown.GetComponent<Button>().enabled = false;
		btnHpDown.GetComponent<Image>().enabled = false;

		btnAtkDown.GetComponent<Button>().enabled = false;
		btnAtkDown.GetComponent<Image>().enabled = false;

		btnDefDown.GetComponent<Button>().enabled = false;
		btnDefDown.GetComponent<Image>().enabled = false;

		playerName.GetComponent<Text>().text = "Shadow";
		txtAvailablePts.GetComponent<Text>().text = "Available Points:";
			

	}

	void Update(){
		
		if(Input.GetKeyDown(KeyCode.K))
		{
			attrScreen.enabled = true;
		}

		if(Input.GetKeyDown(KeyCode.M))
		{
			Debug.Log ("Local HP: " + localtxtHP.ToString());
			Debug.Log ("Local ATK: " + localtxtATK.ToString());
			Debug.Log ("Local DEF: " + localtxtDEF.ToString());
		}

		if(Input.GetKeyDown(KeyCode.L)){
			playerOne.GetComponent<Player>().pool++;
			uiPool.GetComponent<Text>().text = playerOne.GetComponent<Player>().pool.ToString();
			if(playerOne.GetComponent<Player>().pool > 0)
			{
				btnHpUp.GetComponent<Button>().enabled = true;
				btnHpUp.GetComponent<Image>().enabled = true;
				/*btnAtkUp.GetComponent<Button>().enabled = true;
			btnAtkUp.GetComponent<Image>().enabled = true;
			btnDefUp.GetComponent<Button>().enabled = true;
			btnDefUp.GetComponent<Image>().enabled = true;*/
			}
		}


			
	}

	public void ConfirmPts() 
	{
		//Will set allocated points, cannot go back
		attrScreen.enabled = false;
		AudioListener.volume = 1; 
		Debug.Log ("Points have been allocated.");

		localtxtATK = 0;
		localtxtHP = 0;
		localtxtDEF = 0;

		btnHpDown.GetComponent<Button>().enabled = false;
		btnHpDown.GetComponent<Image>().enabled = false;

		btnAtkDown.GetComponent<Button>().enabled = false;
		btnAtkDown.GetComponent<Image>().enabled = false;

		btnDefDown.GetComponent<Button>().enabled = false;
		btnDefDown.GetComponent<Image>().enabled = false;

		playerOne.GetComponent<Player>().pool++;
		uiPool.GetComponent<Text>().text = playerOne.GetComponent<Player>().pool.ToString();

		btnHpUp.GetComponent<Button>().enabled = true;
		btnHpUp.GetComponent<Image>().enabled = true;
		btnAtkUp.GetComponent<Button>().enabled = true;
		btnAtkUp.GetComponent<Image>().enabled = true;
		btnDefUp.GetComponent<Button>().enabled = true;
		btnDefUp.GetComponent<Image>().enabled = true;

		spawner.GetComponentInChildren<Spawner>().startWave();
		
    }

	public void HpUp()
	{
		//Add HP
		Debug.Log ("Added a point in HP");
		playerOne.GetComponent<Player>().atr_hp++;
		playerOne.GetComponent<Player>().hp = playerOne.GetComponent<Player>().startingHealth + (playerOne.GetComponent<Player>().atr_hp * 5);
		uiHP.GetComponent<Text>().text = playerOne.GetComponent<Player>().atr_hp.ToString();
		localtxtHP++;


		//Decrease points in pool
		Debug.Log ("Subtracted a point in Pool");
		playerOne.GetComponent<Player>().pool--;
		uiPool.GetComponent<Text>().text = playerOne.GetComponent<Player>().pool.ToString();

		btnHpDown.GetComponent<Button>().enabled = true;
		btnHpDown.GetComponent<Image>().enabled = true;

		if(playerOne.GetComponent<Player>().pool <= 0)
		{
			btnHpUp.GetComponent<Button>().enabled = false;
			btnHpUp.GetComponent<Image>().enabled = false;
			btnAtkUp.GetComponent<Button>().enabled = false;
			btnAtkUp.GetComponent<Image>().enabled = false;
			btnDefUp.GetComponent<Button>().enabled = false;
			btnDefUp.GetComponent<Image>().enabled = false;
		}
		else if(playerOne.GetComponent<Player>().pool > 0)
		{
			btnHpUp.GetComponent<Button>().enabled = true;
			btnHpUp.GetComponent<Image>().enabled = true;
			/*btnAtkUp.GetComponent<Button>().enabled = true;
			btnAtkUp.GetComponent<Image>().enabled = true;
			btnDefUp.GetComponent<Button>().enabled = true;
			btnDefUp.GetComponent<Image>().enabled = true;*/
		}
			


	}

	public void AtkUp()
	{
		//Add Atk
		Debug.Log ("Added a point in ATK");
		playerOne.GetComponent<Player>().atr_atk++;
		playerOne.GetComponent<Player>().atk = playerOne.GetComponent<Player>().startingDamage + (playerOne.GetComponent<Player>().atr_atk * 2);
		uiATK.GetComponent<Text>().text = playerOne.GetComponent<Player>().atr_atk.ToString();
		localtxtATK++;

		//Decrease points in pool
		Debug.Log ("Subtracted a point in Pool");
		playerOne.GetComponent<Player>().pool--;
		uiPool.GetComponent<Text>().text = playerOne.GetComponent<Player>().pool.ToString();

		btnAtkDown.GetComponent<Button>().enabled = true;
		btnAtkDown.GetComponent<Image>().enabled = true;

		if(playerOne.GetComponent<Player>().pool <= 0)
		{

			btnAtkUp.GetComponent<Button>().enabled = false;
			btnAtkUp.GetComponent<Image>().enabled = false;
			btnDefUp.GetComponent<Button>().enabled = false;
			btnDefUp.GetComponent<Image>().enabled = false;
			btnHpUp.GetComponent<Button>().enabled = false;
			btnHpUp.GetComponent<Image>().enabled = false;
		}

	}

	public void DefUp()
	{
		//Add DEF
		Debug.Log ("Added a point in DEF");
		playerOne.GetComponent<Player>().atr_def++;
		playerOne.GetComponent<Player>().def = playerOne.GetComponent<Player>().atr_def * .25f;
		uiDEF.GetComponent<Text>().text = playerOne.GetComponent<Player>().atr_def.ToString();
		localtxtDEF++;

		//Decrease points in pool
		Debug.Log ("Subtracted a point in Pool");
		playerOne.GetComponent<Player>().pool--;
		uiPool.GetComponent<Text>().text = playerOne.GetComponent<Player>().pool.ToString();

		btnDefDown.GetComponent<Button>().enabled = true;
		btnDefDown.GetComponent<Image>().enabled = true;

		if(playerOne.GetComponent<Player>().pool <= 0)
		{

			btnDefUp.GetComponent<Button>().enabled = false;
			btnDefUp.GetComponent<Image>().enabled = false;
			btnAtkUp.GetComponent<Button>().enabled = false;
			btnAtkUp.GetComponent<Image>().enabled = false;
			btnHpUp.GetComponent<Button>().enabled = false;
			btnHpUp.GetComponent<Image>().enabled = false;
		}
	}

	public void HpDown()
	{
		//Subtract HP
		Debug.Log ("Subtracted a point in HP");
		playerOne.GetComponent<Player>().atr_hp--;
		playerOne.GetComponent<Player>().hp -= 5;
		uiHP.GetComponent<Text>().text = playerOne.GetComponent<Player>().atr_hp.ToString();
		localtxtHP--;

		if(localtxtHP <= 0){
			btnHpDown.GetComponent<Button>().enabled = false;
			btnHpDown.GetComponent<Image>().enabled = false;
		}

		//Increase points in pool
		Debug.Log ("Subtracted a point in Pool");
		playerOne.GetComponent<Player>().pool++;
		uiPool.GetComponent<Text>().text = playerOne.GetComponent<Player>().pool.ToString();

		if(playerOne.GetComponent<Player>().pool > 0){
			btnHpUp.GetComponent<Button>().enabled = true;
			btnHpUp.GetComponent<Image>().enabled = true;
			btnAtkUp.GetComponent<Button>().enabled = true;
			btnAtkUp.GetComponent<Image>().enabled = true;
			btnDefUp.GetComponent<Button>().enabled = true;
			btnDefUp.GetComponent<Image>().enabled = true;
		}
			

	}
	
	public void AtkDown()
	{
		//Subtract atk
		Debug.Log ("Subtracted a point in ATK");
		playerOne.GetComponent<Player>().atr_atk--;
		playerOne.GetComponent<Player>().atk -= 2;
		uiATK.GetComponent<Text>().text = playerOne.GetComponent<Player>().atr_atk.ToString();
		localtxtATK--;

		if(localtxtATK <= 0){
			btnAtkDown.GetComponent<Button>().enabled = false;
			btnAtkDown.GetComponent<Image>().enabled = false;
		}

		//Increase points in pool
		Debug.Log ("Subtracted a point in Pool");
		playerOne.GetComponent<Player>().pool++;
		uiPool.GetComponent<Text>().text = playerOne.GetComponent<Player>().pool.ToString();


		if(playerOne.GetComponent<Player>().pool > 0){
			btnHpUp.GetComponent<Button>().enabled = true;
			btnHpUp.GetComponent<Image>().enabled = true;
			btnAtkUp.GetComponent<Button>().enabled = true;
			btnAtkUp.GetComponent<Image>().enabled = true;
			btnDefUp.GetComponent<Button>().enabled = true;
			btnDefUp.GetComponent<Image>().enabled = true;
		}
	}
	
	public void DefDown()
	{
		//Subtract def
		Debug.Log ("Subtracted a point in DEF");
		playerOne.GetComponent<Player>().atr_def--;
		playerOne.GetComponent<Player>().def -= .25f;
		uiDEF.GetComponent<Text>().text = playerOne.GetComponent<Player>().atr_def.ToString();
		localtxtDEF--;

		if(localtxtDEF <= 0){
			btnDefDown.GetComponent<Button>().enabled = false;
			btnDefDown.GetComponent<Image>().enabled = false;
		}

		//Increase points in pool
		Debug.Log ("Subtracted a point in Pool");
		playerOne.GetComponent<Player>().pool++;
		uiPool.GetComponent<Text>().text = playerOne.GetComponent<Player>().pool.ToString();

		if(playerOne.GetComponent<Player>().pool > 0){
			btnHpUp.GetComponent<Button>().enabled = true;
			btnHpUp.GetComponent<Image>().enabled = true;
			btnAtkUp.GetComponent<Button>().enabled = true;
			btnAtkUp.GetComponent<Image>().enabled = true;
			btnDefUp.GetComponent<Button>().enabled = true;
			btnDefUp.GetComponent<Image>().enabled = true;
		}
	}


}