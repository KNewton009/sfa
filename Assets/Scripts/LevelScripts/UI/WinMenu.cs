﻿using UnityEngine;
using System.Collections;

public class WinMenu : MonoBehaviour {

	private bool winEnabled;
	public bool hasWon;

	// Use this for initialization
	void Start () 
	{
		Debug.Log ("You have won");
	}

	// Update is called once per frame
	void Update () 
	{

		//check if you died in the game
		if (hasWon == true && Application.loadedLevelName == "Game") {

			//Stops the game; shows the menu
			Debug.Log("You have Died");
			winEnabled = true;
			AudioListener.volume = 0;
			Time.timeScale = 0;
			Cursor.visible = true;
		}

	}

	//Shows DeathMenu Options
	private void OnGUI()
	{

		if (winEnabled == true) {
			//Make a background box
			GUI.Box (new Rect (Screen.width / 2 - 100, Screen.height / 2 - 100, 250, 200), "YOU HAVE WON");

			//Main Menu button
			if (GUI.Button (new Rect (Screen.width / 2 - 100, Screen.height / 2 - 50, 250, 50), "Main Menu")) {
				Debug.Log ("You went to MainMenu");
				Application.LoadLevel ("mainMenu");
			}

			//Restart the level button
			if (GUI.Button (new Rect (Screen.width / 2 - 100, Screen.height / 2, 250, 50), "Restart Game")) {
				Debug.Log ("You have restarted");
				Application.LoadLevel (Application.loadedLevelName);
			}

			//Quit game button
			if (GUI.Button (new Rect (Screen.width / 2 - 100, Screen.height / 2 + 50, 250, 50), "Quit Game")) {
				//Intended use is for standalone quit
				Debug.Log ("You Quit the game");
				Application.Quit ();

			}
		}
	}
}
