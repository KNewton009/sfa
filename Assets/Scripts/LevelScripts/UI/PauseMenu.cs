﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour {

	private bool pauseEnabled = false;
	private bool showGraphicsDropDown = false;
	private bool showVolSettingsDropDown = false;
	private bool isMuted;

	// Use this for initialization
	void Start () 
	{

		pauseEnabled = false;
		Time.timeScale = 1;
		AudioListener.volume = 1;
		isMuted = false;
	}
	
	// Update is called once per frame
	void Update () 
	{

		//check if pause button (escape key) is pressed
		if (Input.GetKeyDown ("escape") && Application.loadedLevelName == "Game") 
		{
			Debug.Log ("You can now Pause");
			
			//check if game is already paused		
			if (pauseEnabled == true) 
			{
				//unpause the game
				Debug.Log ("You have unPaused");
				pauseEnabled = false;
				Time.timeScale = 1;
				AudioListener.volume = 1;
				//Cursor.visible = false;			
			}
			
			//else if game isn't paused, then pause it
			else if (pauseEnabled == false) 
			{
				Debug.Log("You have Paused");
				pauseEnabled = true;
				AudioListener.volume = 0;
				Time.timeScale = 0;
				//Cursor.visible = true;
			}
			
		}

	}

	//Shows PauseMenu Options
	private void OnGUI()
	{
		
		if(pauseEnabled == true)
		{
			//Make a background box
			GUI.Box(new Rect(Screen.width /2 - 100,Screen.height /2 - 150,250,300), "Pause Menu");
			
			//Main Menu button
			if(GUI.Button(new Rect(Screen.width /2 - 100,Screen.height /2 - 100,250,50), "Main Menu"))
			{
				Debug.Log("You went to MainMenu");
				Application.LoadLevel("mainMenu");
			}

			//Restart button
			if(GUI.Button(new Rect(Screen.width /2 - 100,Screen.height /2 - 50,250,50), "Restart"))
			{
				Debug.Log("You Restarted");
				Application.LoadLevel(Application.loadedLevel);
			}
			
			//Change Graphics Quality button
			if(GUI.Button(new Rect(Screen.width /2 - 100,Screen.height /2 ,250,50), "Change Graphics Quality"))
			{
				if(showGraphicsDropDown == false)
				{
					showGraphicsDropDown = true;
				}
				else
				{
					showGraphicsDropDown = false;
				}
			}

			//Change Volume Settings button
			if(GUI.Button(new Rect(Screen.width /2 - 100,Screen.height /2 + 50,250,50), "Volume Settings"))
			{
				if(showVolSettingsDropDown == false)
				{
					showVolSettingsDropDown = true;
				}
				else
				{
					showVolSettingsDropDown = false;
				}
			}
			
			//Quit game button
			if (GUI.Button (new Rect (Screen.width /2 - 100,Screen.height /2 + 100,250,50), "Quit Game"))
			{
				Debug.Log("You Quit the game");
				Application.Quit();
			}
			
			//Create the Graphics settings buttons 
			if(showGraphicsDropDown == true)
			{
				if(GUI.Button(new Rect(Screen.width /2 + 150,Screen.height /2 ,250,50), "Fastest"))
				{
					QualitySettings.SetQualityLevel(0);
				}

				if(GUI.Button(new Rect(Screen.width /2 + 150,Screen.height /2 + 50,250,50), "Fast"))
				{
					QualitySettings.SetQualityLevel(1);
				}

				if(GUI.Button(new Rect(Screen.width /2 + 150,Screen.height /2 + 100,250,50), "Simple"))
				{
					QualitySettings.SetQualityLevel(2);
				}

				if(GUI.Button(new Rect(Screen.width /2 + 150,Screen.height /2 + 150,250,50), "Good"))
				{
					QualitySettings.SetQualityLevel(3);
				}

				if(GUI.Button(new Rect(Screen.width /2 + 150,Screen.height /2 + 200,250,50), "Beautiful"))
				{
					QualitySettings.SetQualityLevel(4);
				}

				if(GUI.Button(new Rect(Screen.width /2 + 150,Screen.height /2 + 250,250,50), "Fantastic"))
				{
					QualitySettings.SetQualityLevel(5);
				}

				if(Input.GetKeyDown("escape"))
				{
					showGraphicsDropDown = false;
				}
				
			}

			//Create the Volume settings 
			if(showVolSettingsDropDown == true)
			{
				if(GUI.Button(new Rect(Screen.width /2 + 150,Screen.height /2 ,250,50), "Mute (Cannot unMute)"))
				{
					print ("Muted");
					AudioListener.volume = 0;
					isMuted = true;

					//Not functional but is pretty much what suppose to happen
					/*if (isMuted == true && this.GetComponent<Button>().onClick) {
						print ("Volume has returned");
						AudioListener.volume = 1;
						isMuted = false;*/
				}

				if(Input.GetKeyDown("escape"))
				{
					showVolSettingsDropDown = false;
				}

			}

				
		}
	}
}

